public enum ConditionType {
    EQUAL("eq"),
    NOT_EQUAL("ne"),
    GREATER_THAN("sgt"),
    GREATER_OR_EQUAL("sge"),
    LESS_THAN("slt"),
    LESS_OR_EQUAL("sle");

    private String value;

    ConditionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
