// Generated from superlang/Superlang.g4 by ANTLR 4.7.1
package superlang;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SuperlangParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		READ=1, WRITE=2, TABLICA=3, TYP=4, IF=5, THEN=6, ENDIF=7, WHILE=8, ENDWHILE=9, 
		FUNCTION=10, ENDFUNCTION=11, STRUCT=12, ENDSTRUCT=13, CALL=14, OPENING_BRACKET=15, 
		CLOSING_BRACKET=16, OPENING_SQUARE_BRACKET=17, CLOSING_SQUARE_BRACKET=18, 
		COLON=19, DOT=20, TOINT=21, TOREAL=22, ADD=23, SUB=24, MULT=25, DIV=26, 
		NEWLINE=27, IS_EQUAL=28, EQUAL=29, NOT_EQUAL=30, LESSER_THAN=31, LESSER_OR_EQUAL_THAN=32, 
		GREATER_THAN=33, GREATER_OR_EQUAL_THAN=34, STRING=35, ID=36, INT=37, REAL=38, 
		WS=39, COMMENT=40;
	public static final int
		RULE_prog = 0, RULE_block = 1, RULE_functionDefinition = 2, RULE_structDefinition = 3, 
		RULE_stat = 4, RULE_expr1 = 5, RULE_expr2 = 6, RULE_exprBasic = 7, RULE_condition = 8, 
		RULE_ifBlock = 9, RULE_loopBlock = 10, RULE_functionBlock = 11, RULE_structFields = 12;
	public static final String[] ruleNames = {
		"prog", "block", "functionDefinition", "structDefinition", "stat", "expr1", 
		"expr2", "exprBasic", "condition", "ifBlock", "loopBlock", "functionBlock", 
		"structFields"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'CZYTAJ'", "'PISZ'", "'TAB'", null, "'JEZELI'", "'WTEDY'", "'KONJEZELI'", 
		"'PETLA'", "'KONPETLA'", "'FUNKCJA'", "'KONFUNKCJA'", "'STRUKTURA'", "'KONSTRUKTURA'", 
		"'WYWOLAJ'", "'('", "')'", "'['", "']'", "':'", "'.'", "'(cal)'", "'(rz)'", 
		"'+'", "'-'", "'*'", "'/'", null, "'='", "'=='", "'~='", "'<'", "'<='", 
		"'>'", "'>='"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "READ", "WRITE", "TABLICA", "TYP", "IF", "THEN", "ENDIF", "WHILE", 
		"ENDWHILE", "FUNCTION", "ENDFUNCTION", "STRUCT", "ENDSTRUCT", "CALL", 
		"OPENING_BRACKET", "CLOSING_BRACKET", "OPENING_SQUARE_BRACKET", "CLOSING_SQUARE_BRACKET", 
		"COLON", "DOT", "TOINT", "TOREAL", "ADD", "SUB", "MULT", "DIV", "NEWLINE", 
		"IS_EQUAL", "EQUAL", "NOT_EQUAL", "LESSER_THAN", "LESSER_OR_EQUAL_THAN", 
		"GREATER_THAN", "GREATER_OR_EQUAL_THAN", "STRING", "ID", "INT", "REAL", 
		"WS", "COMMENT"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Superlang.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SuperlangParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(SuperlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperlangParser.NEWLINE, i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public List<FunctionDefinitionContext> functionDefinition() {
			return getRuleContexts(FunctionDefinitionContext.class);
		}
		public FunctionDefinitionContext functionDefinition(int i) {
			return getRuleContext(FunctionDefinitionContext.class,i);
		}
		public List<StructDefinitionContext> structDefinition() {
			return getRuleContexts(StructDefinitionContext.class);
		}
		public StructDefinitionContext structDefinition(int i) {
			return getRuleContext(StructDefinitionContext.class,i);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterProg(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitProg(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(34);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << READ) | (1L << WRITE) | (1L << IF) | (1L << WHILE) | (1L << FUNCTION) | (1L << STRUCT) | (1L << CALL) | (1L << NEWLINE) | (1L << ID) | (1L << COMMENT))) != 0)) {
				{
				{
				setState(29);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case READ:
				case WRITE:
				case IF:
				case WHILE:
				case CALL:
				case ID:
				case COMMENT:
					{
					setState(26);
					stat();
					}
					break;
				case FUNCTION:
					{
					setState(27);
					functionDefinition();
					}
					break;
				case STRUCT:
					{
					setState(28);
					structDefinition();
					}
					break;
				case NEWLINE:
					break;
				default:
					break;
				}
				setState(31);
				match(NEWLINE);
				}
				}
				setState(36);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(SuperlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperlangParser.NEWLINE, i);
		}
		public List<StatContext> stat() {
			return getRuleContexts(StatContext.class);
		}
		public StatContext stat(int i) {
			return getRuleContext(StatContext.class,i);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitBlock(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_block);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(43);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << READ) | (1L << WRITE) | (1L << IF) | (1L << WHILE) | (1L << CALL) | (1L << NEWLINE) | (1L << ID) | (1L << COMMENT))) != 0)) {
				{
				{
				setState(38);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << READ) | (1L << WRITE) | (1L << IF) | (1L << WHILE) | (1L << CALL) | (1L << ID) | (1L << COMMENT))) != 0)) {
					{
					setState(37);
					stat();
					}
				}

				setState(40);
				match(NEWLINE);
				}
				}
				setState(45);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionDefinitionContext extends ParserRuleContext {
		public TerminalNode FUNCTION() { return getToken(SuperlangParser.FUNCTION, 0); }
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public TerminalNode COLON() { return getToken(SuperlangParser.COLON, 0); }
		public TerminalNode NEWLINE() { return getToken(SuperlangParser.NEWLINE, 0); }
		public FunctionBlockContext functionBlock() {
			return getRuleContext(FunctionBlockContext.class,0);
		}
		public TerminalNode ENDFUNCTION() { return getToken(SuperlangParser.ENDFUNCTION, 0); }
		public FunctionDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterFunctionDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitFunctionDefinition(this);
		}
	}

	public final FunctionDefinitionContext functionDefinition() throws RecognitionException {
		FunctionDefinitionContext _localctx = new FunctionDefinitionContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_functionDefinition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46);
			match(FUNCTION);
			setState(47);
			match(ID);
			setState(48);
			match(COLON);
			setState(49);
			match(NEWLINE);
			setState(50);
			functionBlock();
			setState(51);
			match(ENDFUNCTION);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StructDefinitionContext extends ParserRuleContext {
		public TerminalNode STRUCT() { return getToken(SuperlangParser.STRUCT, 0); }
		public List<TerminalNode> ID() { return getTokens(SuperlangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SuperlangParser.ID, i);
		}
		public List<TerminalNode> COLON() { return getTokens(SuperlangParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(SuperlangParser.COLON, i);
		}
		public List<TerminalNode> NEWLINE() { return getTokens(SuperlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperlangParser.NEWLINE, i);
		}
		public TerminalNode ENDSTRUCT() { return getToken(SuperlangParser.ENDSTRUCT, 0); }
		public List<TerminalNode> TYP() { return getTokens(SuperlangParser.TYP); }
		public TerminalNode TYP(int i) {
			return getToken(SuperlangParser.TYP, i);
		}
		public StructDefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structDefinition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterStructDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitStructDefinition(this);
		}
	}

	public final StructDefinitionContext structDefinition() throws RecognitionException {
		StructDefinitionContext _localctx = new StructDefinitionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_structDefinition);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(53);
			match(STRUCT);
			setState(54);
			match(ID);
			setState(55);
			match(COLON);
			setState(56);
			match(NEWLINE);
			setState(65);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TYP || _la==NEWLINE) {
				{
				{
				setState(60);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==TYP) {
					{
					setState(57);
					match(TYP);
					setState(58);
					match(COLON);
					setState(59);
					match(ID);
					}
				}

				setState(62);
				match(NEWLINE);
				}
				}
				setState(67);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(68);
			match(ENDSTRUCT);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StatContext extends ParserRuleContext {
		public StatContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stat; }
	 
		public StatContext() { }
		public void copyFrom(StatContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class ReadContext extends StatContext {
		public TerminalNode READ() { return getToken(SuperlangParser.READ, 0); }
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public ReadContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterRead(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitRead(this);
		}
	}
	public static class ConditionalContext extends StatContext {
		public TerminalNode IF() { return getToken(SuperlangParser.IF, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public TerminalNode THEN() { return getToken(SuperlangParser.THEN, 0); }
		public IfBlockContext ifBlock() {
			return getRuleContext(IfBlockContext.class,0);
		}
		public TerminalNode ENDIF() { return getToken(SuperlangParser.ENDIF, 0); }
		public ConditionalContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterConditional(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitConditional(this);
		}
	}
	public static class AssignElementTablicyContext extends StatContext {
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public TerminalNode OPENING_SQUARE_BRACKET() { return getToken(SuperlangParser.OPENING_SQUARE_BRACKET, 0); }
		public TerminalNode INT() { return getToken(SuperlangParser.INT, 0); }
		public TerminalNode CLOSING_SQUARE_BRACKET() { return getToken(SuperlangParser.CLOSING_SQUARE_BRACKET, 0); }
		public TerminalNode IS_EQUAL() { return getToken(SuperlangParser.IS_EQUAL, 0); }
		public Expr1Context expr1() {
			return getRuleContext(Expr1Context.class,0);
		}
		public AssignElementTablicyContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterAssignElementTablicy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitAssignElementTablicy(this);
		}
	}
	public static class LoopContext extends StatContext {
		public TerminalNode WHILE() { return getToken(SuperlangParser.WHILE, 0); }
		public ConditionContext condition() {
			return getRuleContext(ConditionContext.class,0);
		}
		public TerminalNode THEN() { return getToken(SuperlangParser.THEN, 0); }
		public LoopBlockContext loopBlock() {
			return getRuleContext(LoopBlockContext.class,0);
		}
		public TerminalNode ENDWHILE() { return getToken(SuperlangParser.ENDWHILE, 0); }
		public LoopContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterLoop(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitLoop(this);
		}
	}
	public static class AssignElementStrukturyContext extends StatContext {
		public List<TerminalNode> ID() { return getTokens(SuperlangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SuperlangParser.ID, i);
		}
		public TerminalNode DOT() { return getToken(SuperlangParser.DOT, 0); }
		public TerminalNode IS_EQUAL() { return getToken(SuperlangParser.IS_EQUAL, 0); }
		public Expr1Context expr1() {
			return getRuleContext(Expr1Context.class,0);
		}
		public AssignElementStrukturyContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterAssignElementStruktury(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitAssignElementStruktury(this);
		}
	}
	public static class CallFunctionContext extends StatContext {
		public TerminalNode CALL() { return getToken(SuperlangParser.CALL, 0); }
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public TerminalNode OPENING_BRACKET() { return getToken(SuperlangParser.OPENING_BRACKET, 0); }
		public TerminalNode CLOSING_BRACKET() { return getToken(SuperlangParser.CLOSING_BRACKET, 0); }
		public CallFunctionContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterCallFunction(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitCallFunction(this);
		}
	}
	public static class CommentContext extends StatContext {
		public TerminalNode COMMENT() { return getToken(SuperlangParser.COMMENT, 0); }
		public CommentContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterComment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitComment(this);
		}
	}
	public static class WriteContext extends StatContext {
		public TerminalNode WRITE() { return getToken(SuperlangParser.WRITE, 0); }
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public WriteContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterWrite(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitWrite(this);
		}
	}
	public static class AssignContext extends StatContext {
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public TerminalNode IS_EQUAL() { return getToken(SuperlangParser.IS_EQUAL, 0); }
		public Expr1Context expr1() {
			return getRuleContext(Expr1Context.class,0);
		}
		public AssignContext(StatContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterAssign(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitAssign(this);
		}
	}

	public final StatContext stat() throws RecognitionException {
		StatContext _localctx = new StatContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_stat);
		try {
			setState(105);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				_localctx = new WriteContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(70);
				match(WRITE);
				setState(71);
				match(ID);
				}
				break;
			case 2:
				_localctx = new ReadContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(72);
				match(READ);
				setState(73);
				match(ID);
				}
				break;
			case 3:
				_localctx = new ConditionalContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(74);
				match(IF);
				setState(75);
				condition();
				setState(76);
				match(THEN);
				setState(77);
				ifBlock();
				setState(78);
				match(ENDIF);
				}
				break;
			case 4:
				_localctx = new LoopContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(80);
				match(WHILE);
				setState(81);
				condition();
				setState(82);
				match(THEN);
				setState(83);
				loopBlock();
				setState(84);
				match(ENDWHILE);
				}
				break;
			case 5:
				_localctx = new AssignContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(86);
				match(ID);
				setState(87);
				match(IS_EQUAL);
				setState(88);
				expr1();
				}
				break;
			case 6:
				_localctx = new AssignElementTablicyContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(89);
				match(ID);
				setState(90);
				match(OPENING_SQUARE_BRACKET);
				setState(91);
				match(INT);
				setState(92);
				match(CLOSING_SQUARE_BRACKET);
				setState(93);
				match(IS_EQUAL);
				setState(94);
				expr1();
				}
				break;
			case 7:
				_localctx = new AssignElementStrukturyContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(95);
				match(ID);
				setState(96);
				match(DOT);
				setState(97);
				match(ID);
				setState(98);
				match(IS_EQUAL);
				setState(99);
				expr1();
				}
				break;
			case 8:
				_localctx = new CallFunctionContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(100);
				match(CALL);
				setState(101);
				match(ID);
				setState(102);
				match(OPENING_BRACKET);
				setState(103);
				match(CLOSING_BRACKET);
				}
				break;
			case 9:
				_localctx = new CommentContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(104);
				match(COMMENT);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr1Context extends ParserRuleContext {
		public Expr1Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr1; }
	 
		public Expr1Context() { }
		public void copyFrom(Expr1Context ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AddContext extends Expr1Context {
		public List<Expr2Context> expr2() {
			return getRuleContexts(Expr2Context.class);
		}
		public Expr2Context expr2(int i) {
			return getRuleContext(Expr2Context.class,i);
		}
		public TerminalNode ADD() { return getToken(SuperlangParser.ADD, 0); }
		public AddContext(Expr1Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterAdd(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitAdd(this);
		}
	}
	public static class SubContext extends Expr1Context {
		public List<Expr2Context> expr2() {
			return getRuleContexts(Expr2Context.class);
		}
		public Expr2Context expr2(int i) {
			return getRuleContext(Expr2Context.class,i);
		}
		public TerminalNode SUB() { return getToken(SuperlangParser.SUB, 0); }
		public SubContext(Expr1Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterSub(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitSub(this);
		}
	}
	public static class SingleExpr2Context extends Expr1Context {
		public Expr2Context expr2() {
			return getRuleContext(Expr2Context.class,0);
		}
		public SingleExpr2Context(Expr1Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterSingleExpr2(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitSingleExpr2(this);
		}
	}

	public final Expr1Context expr1() throws RecognitionException {
		Expr1Context _localctx = new Expr1Context(_ctx, getState());
		enterRule(_localctx, 10, RULE_expr1);
		try {
			setState(116);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
			case 1:
				_localctx = new SingleExpr2Context(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(107);
				expr2();
				}
				break;
			case 2:
				_localctx = new AddContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(108);
				expr2();
				setState(109);
				match(ADD);
				setState(110);
				expr2();
				}
				break;
			case 3:
				_localctx = new SubContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(112);
				expr2();
				setState(113);
				match(SUB);
				setState(114);
				expr2();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Expr2Context extends ParserRuleContext {
		public Expr2Context(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr2; }
	 
		public Expr2Context() { }
		public void copyFrom(Expr2Context ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class DivContext extends Expr2Context {
		public List<ExprBasicContext> exprBasic() {
			return getRuleContexts(ExprBasicContext.class);
		}
		public ExprBasicContext exprBasic(int i) {
			return getRuleContext(ExprBasicContext.class,i);
		}
		public TerminalNode DIV() { return getToken(SuperlangParser.DIV, 0); }
		public DivContext(Expr2Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterDiv(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitDiv(this);
		}
	}
	public static class MultContext extends Expr2Context {
		public List<ExprBasicContext> exprBasic() {
			return getRuleContexts(ExprBasicContext.class);
		}
		public ExprBasicContext exprBasic(int i) {
			return getRuleContext(ExprBasicContext.class,i);
		}
		public TerminalNode MULT() { return getToken(SuperlangParser.MULT, 0); }
		public MultContext(Expr2Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterMult(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitMult(this);
		}
	}
	public static class SingleExprBasicContext extends Expr2Context {
		public ExprBasicContext exprBasic() {
			return getRuleContext(ExprBasicContext.class,0);
		}
		public SingleExprBasicContext(Expr2Context ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterSingleExprBasic(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitSingleExprBasic(this);
		}
	}

	public final Expr2Context expr2() throws RecognitionException {
		Expr2Context _localctx = new Expr2Context(_ctx, getState());
		enterRule(_localctx, 12, RULE_expr2);
		try {
			setState(127);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				_localctx = new SingleExprBasicContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(118);
				exprBasic();
				}
				break;
			case 2:
				_localctx = new MultContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(119);
				exprBasic();
				setState(120);
				match(MULT);
				setState(121);
				exprBasic();
				}
				break;
			case 3:
				_localctx = new DivContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(123);
				exprBasic();
				setState(124);
				match(DIV);
				setState(125);
				exprBasic();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprBasicContext extends ParserRuleContext {
		public ExprBasicContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_exprBasic; }
	 
		public ExprBasicContext() { }
		public void copyFrom(ExprBasicContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class TablicaContext extends ExprBasicContext {
		public TerminalNode TABLICA() { return getToken(SuperlangParser.TABLICA, 0); }
		public TerminalNode TYP() { return getToken(SuperlangParser.TYP, 0); }
		public TerminalNode OPENING_SQUARE_BRACKET() { return getToken(SuperlangParser.OPENING_SQUARE_BRACKET, 0); }
		public TerminalNode INT() { return getToken(SuperlangParser.INT, 0); }
		public TerminalNode CLOSING_SQUARE_BRACKET() { return getToken(SuperlangParser.CLOSING_SQUARE_BRACKET, 0); }
		public TablicaContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterTablica(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitTablica(this);
		}
	}
	public static class ToIntContext extends ExprBasicContext {
		public TerminalNode TOINT() { return getToken(SuperlangParser.TOINT, 0); }
		public ExprBasicContext exprBasic() {
			return getRuleContext(ExprBasicContext.class,0);
		}
		public ToIntContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterToInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitToInt(this);
		}
	}
	public static class StringContext extends ExprBasicContext {
		public TerminalNode STRING() { return getToken(SuperlangParser.STRING, 0); }
		public StringContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterString(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitString(this);
		}
	}
	public static class ElementStrukturyContext extends ExprBasicContext {
		public List<TerminalNode> ID() { return getTokens(SuperlangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SuperlangParser.ID, i);
		}
		public TerminalNode DOT() { return getToken(SuperlangParser.DOT, 0); }
		public ElementStrukturyContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterElementStruktury(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitElementStruktury(this);
		}
	}
	public static class StrukturaContext extends ExprBasicContext {
		public TerminalNode STRUCT() { return getToken(SuperlangParser.STRUCT, 0); }
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public StrukturaContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterStruktura(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitStruktura(this);
		}
	}
	public static class IdContext extends ExprBasicContext {
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public IdContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterId(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitId(this);
		}
	}
	public static class RealContext extends ExprBasicContext {
		public TerminalNode REAL() { return getToken(SuperlangParser.REAL, 0); }
		public RealContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterReal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitReal(this);
		}
	}
	public static class ToRealContext extends ExprBasicContext {
		public TerminalNode TOREAL() { return getToken(SuperlangParser.TOREAL, 0); }
		public ExprBasicContext exprBasic() {
			return getRuleContext(ExprBasicContext.class,0);
		}
		public ToRealContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterToReal(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitToReal(this);
		}
	}
	public static class ElementTablicyContext extends ExprBasicContext {
		public TerminalNode ID() { return getToken(SuperlangParser.ID, 0); }
		public TerminalNode OPENING_SQUARE_BRACKET() { return getToken(SuperlangParser.OPENING_SQUARE_BRACKET, 0); }
		public TerminalNode INT() { return getToken(SuperlangParser.INT, 0); }
		public TerminalNode CLOSING_SQUARE_BRACKET() { return getToken(SuperlangParser.CLOSING_SQUARE_BRACKET, 0); }
		public ElementTablicyContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterElementTablicy(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitElementTablicy(this);
		}
	}
	public static class IntContext extends ExprBasicContext {
		public TerminalNode INT() { return getToken(SuperlangParser.INT, 0); }
		public IntContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterInt(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitInt(this);
		}
	}
	public static class BracketsContext extends ExprBasicContext {
		public TerminalNode OPENING_BRACKET() { return getToken(SuperlangParser.OPENING_BRACKET, 0); }
		public Expr1Context expr1() {
			return getRuleContext(Expr1Context.class,0);
		}
		public TerminalNode CLOSING_BRACKET() { return getToken(SuperlangParser.CLOSING_BRACKET, 0); }
		public BracketsContext(ExprBasicContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterBrackets(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitBrackets(this);
		}
	}

	public final ExprBasicContext exprBasic() throws RecognitionException {
		ExprBasicContext _localctx = new ExprBasicContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_exprBasic);
		try {
			setState(155);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new IdContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(129);
				match(ID);
				}
				break;
			case 2:
				_localctx = new IntContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(130);
				match(INT);
				}
				break;
			case 3:
				_localctx = new RealContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(131);
				match(REAL);
				}
				break;
			case 4:
				_localctx = new ToIntContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(132);
				match(TOINT);
				setState(133);
				exprBasic();
				}
				break;
			case 5:
				_localctx = new ToRealContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(134);
				match(TOREAL);
				setState(135);
				exprBasic();
				}
				break;
			case 6:
				_localctx = new BracketsContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(136);
				match(OPENING_BRACKET);
				setState(137);
				expr1();
				setState(138);
				match(CLOSING_BRACKET);
				}
				break;
			case 7:
				_localctx = new StringContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(140);
				match(STRING);
				}
				break;
			case 8:
				_localctx = new TablicaContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(141);
				match(TABLICA);
				setState(142);
				match(TYP);
				setState(143);
				match(OPENING_SQUARE_BRACKET);
				setState(144);
				match(INT);
				setState(145);
				match(CLOSING_SQUARE_BRACKET);
				}
				break;
			case 9:
				_localctx = new ElementTablicyContext(_localctx);
				enterOuterAlt(_localctx, 9);
				{
				setState(146);
				match(ID);
				setState(147);
				match(OPENING_SQUARE_BRACKET);
				setState(148);
				match(INT);
				setState(149);
				match(CLOSING_SQUARE_BRACKET);
				}
				break;
			case 10:
				_localctx = new StrukturaContext(_localctx);
				enterOuterAlt(_localctx, 10);
				{
				setState(150);
				match(STRUCT);
				setState(151);
				match(ID);
				}
				break;
			case 11:
				_localctx = new ElementStrukturyContext(_localctx);
				enterOuterAlt(_localctx, 11);
				{
				setState(152);
				match(ID);
				setState(153);
				match(DOT);
				setState(154);
				match(ID);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConditionContext extends ParserRuleContext {
		public ConditionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_condition; }
	 
		public ConditionContext() { }
		public void copyFrom(ConditionContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class LessOrEqualCondContext extends ConditionContext {
		public List<Expr1Context> expr1() {
			return getRuleContexts(Expr1Context.class);
		}
		public Expr1Context expr1(int i) {
			return getRuleContext(Expr1Context.class,i);
		}
		public TerminalNode LESSER_OR_EQUAL_THAN() { return getToken(SuperlangParser.LESSER_OR_EQUAL_THAN, 0); }
		public LessOrEqualCondContext(ConditionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterLessOrEqualCond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitLessOrEqualCond(this);
		}
	}
	public static class GreaterThanCondContext extends ConditionContext {
		public List<Expr1Context> expr1() {
			return getRuleContexts(Expr1Context.class);
		}
		public Expr1Context expr1(int i) {
			return getRuleContext(Expr1Context.class,i);
		}
		public TerminalNode GREATER_THAN() { return getToken(SuperlangParser.GREATER_THAN, 0); }
		public GreaterThanCondContext(ConditionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterGreaterThanCond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitGreaterThanCond(this);
		}
	}
	public static class EqualCondContext extends ConditionContext {
		public List<Expr1Context> expr1() {
			return getRuleContexts(Expr1Context.class);
		}
		public Expr1Context expr1(int i) {
			return getRuleContext(Expr1Context.class,i);
		}
		public TerminalNode EQUAL() { return getToken(SuperlangParser.EQUAL, 0); }
		public EqualCondContext(ConditionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterEqualCond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitEqualCond(this);
		}
	}
	public static class LessThanCondContext extends ConditionContext {
		public List<Expr1Context> expr1() {
			return getRuleContexts(Expr1Context.class);
		}
		public Expr1Context expr1(int i) {
			return getRuleContext(Expr1Context.class,i);
		}
		public TerminalNode LESSER_THAN() { return getToken(SuperlangParser.LESSER_THAN, 0); }
		public LessThanCondContext(ConditionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterLessThanCond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitLessThanCond(this);
		}
	}
	public static class GreaterOrEqualCondContext extends ConditionContext {
		public List<Expr1Context> expr1() {
			return getRuleContexts(Expr1Context.class);
		}
		public Expr1Context expr1(int i) {
			return getRuleContext(Expr1Context.class,i);
		}
		public TerminalNode GREATER_OR_EQUAL_THAN() { return getToken(SuperlangParser.GREATER_OR_EQUAL_THAN, 0); }
		public GreaterOrEqualCondContext(ConditionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterGreaterOrEqualCond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitGreaterOrEqualCond(this);
		}
	}
	public static class NotEqualCondContext extends ConditionContext {
		public List<Expr1Context> expr1() {
			return getRuleContexts(Expr1Context.class);
		}
		public Expr1Context expr1(int i) {
			return getRuleContext(Expr1Context.class,i);
		}
		public TerminalNode NOT_EQUAL() { return getToken(SuperlangParser.NOT_EQUAL, 0); }
		public NotEqualCondContext(ConditionContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterNotEqualCond(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitNotEqualCond(this);
		}
	}

	public final ConditionContext condition() throws RecognitionException {
		ConditionContext _localctx = new ConditionContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_condition);
		try {
			setState(181);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,10,_ctx) ) {
			case 1:
				_localctx = new EqualCondContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(157);
				expr1();
				setState(158);
				match(EQUAL);
				setState(159);
				expr1();
				}
				break;
			case 2:
				_localctx = new NotEqualCondContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(161);
				expr1();
				setState(162);
				match(NOT_EQUAL);
				setState(163);
				expr1();
				}
				break;
			case 3:
				_localctx = new LessThanCondContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(165);
				expr1();
				setState(166);
				match(LESSER_THAN);
				setState(167);
				expr1();
				}
				break;
			case 4:
				_localctx = new LessOrEqualCondContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(169);
				expr1();
				setState(170);
				match(LESSER_OR_EQUAL_THAN);
				setState(171);
				expr1();
				}
				break;
			case 5:
				_localctx = new GreaterThanCondContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(173);
				expr1();
				setState(174);
				match(GREATER_THAN);
				setState(175);
				expr1();
				}
				break;
			case 6:
				_localctx = new GreaterOrEqualCondContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(177);
				expr1();
				setState(178);
				match(GREATER_OR_EQUAL_THAN);
				setState(179);
				expr1();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class IfBlockContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public IfBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ifBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterIfBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitIfBlock(this);
		}
	}

	public final IfBlockContext ifBlock() throws RecognitionException {
		IfBlockContext _localctx = new IfBlockContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_ifBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(183);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class LoopBlockContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public LoopBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_loopBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterLoopBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitLoopBlock(this);
		}
	}

	public final LoopBlockContext loopBlock() throws RecognitionException {
		LoopBlockContext _localctx = new LoopBlockContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_loopBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(185);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FunctionBlockContext extends ParserRuleContext {
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public FunctionBlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_functionBlock; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterFunctionBlock(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitFunctionBlock(this);
		}
	}

	public final FunctionBlockContext functionBlock() throws RecognitionException {
		FunctionBlockContext _localctx = new FunctionBlockContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_functionBlock);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(187);
			block();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StructFieldsContext extends ParserRuleContext {
		public List<TerminalNode> NEWLINE() { return getTokens(SuperlangParser.NEWLINE); }
		public TerminalNode NEWLINE(int i) {
			return getToken(SuperlangParser.NEWLINE, i);
		}
		public List<TerminalNode> TYP() { return getTokens(SuperlangParser.TYP); }
		public TerminalNode TYP(int i) {
			return getToken(SuperlangParser.TYP, i);
		}
		public List<TerminalNode> COLON() { return getTokens(SuperlangParser.COLON); }
		public TerminalNode COLON(int i) {
			return getToken(SuperlangParser.COLON, i);
		}
		public List<TerminalNode> ID() { return getTokens(SuperlangParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SuperlangParser.ID, i);
		}
		public StructFieldsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_structFields; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).enterStructFields(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SuperlangListener ) ((SuperlangListener)listener).exitStructFields(this);
		}
	}

	public final StructFieldsContext structFields() throws RecognitionException {
		StructFieldsContext _localctx = new StructFieldsContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_structFields);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(195); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(192);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la==TYP) {
					{
					setState(189);
					match(TYP);
					setState(190);
					match(COLON);
					setState(191);
					match(ID);
					}
				}

				setState(194);
				match(NEWLINE);
				}
				}
				setState(197); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( _la==TYP || _la==NEWLINE );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3*\u00ca\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\3\2\3\2\3\2\5\2 \n\2\3\2\7\2#\n\2\f\2\16"+
		"\2&\13\2\3\3\5\3)\n\3\3\3\7\3,\n\3\f\3\16\3/\13\3\3\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5?\n\5\3\5\7\5B\n\5\f\5\16\5E\13"+
		"\5\3\5\3\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6"+
		"\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3"+
		"\6\3\6\3\6\5\6l\n\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3\7\5\7w\n\7\3\b\3"+
		"\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u0082\n\b\3\t\3\t\3\t\3\t\3\t\3\t\3"+
		"\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\3\t\3\t\5\t\u009e\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u00b8\n\n\3\13\3"+
		"\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\5\16\u00c3\n\16\3\16\6\16\u00c6\n\16"+
		"\r\16\16\16\u00c7\3\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2\2\u00e1"+
		"\2$\3\2\2\2\4-\3\2\2\2\6\60\3\2\2\2\b\67\3\2\2\2\nk\3\2\2\2\fv\3\2\2\2"+
		"\16\u0081\3\2\2\2\20\u009d\3\2\2\2\22\u00b7\3\2\2\2\24\u00b9\3\2\2\2\26"+
		"\u00bb\3\2\2\2\30\u00bd\3\2\2\2\32\u00c5\3\2\2\2\34 \5\n\6\2\35 \5\6\4"+
		"\2\36 \5\b\5\2\37\34\3\2\2\2\37\35\3\2\2\2\37\36\3\2\2\2\37 \3\2\2\2 "+
		"!\3\2\2\2!#\7\35\2\2\"\37\3\2\2\2#&\3\2\2\2$\"\3\2\2\2$%\3\2\2\2%\3\3"+
		"\2\2\2&$\3\2\2\2\')\5\n\6\2(\'\3\2\2\2()\3\2\2\2)*\3\2\2\2*,\7\35\2\2"+
		"+(\3\2\2\2,/\3\2\2\2-+\3\2\2\2-.\3\2\2\2.\5\3\2\2\2/-\3\2\2\2\60\61\7"+
		"\f\2\2\61\62\7&\2\2\62\63\7\25\2\2\63\64\7\35\2\2\64\65\5\30\r\2\65\66"+
		"\7\r\2\2\66\7\3\2\2\2\678\7\16\2\289\7&\2\29:\7\25\2\2:C\7\35\2\2;<\7"+
		"\6\2\2<=\7\25\2\2=?\7&\2\2>;\3\2\2\2>?\3\2\2\2?@\3\2\2\2@B\7\35\2\2A>"+
		"\3\2\2\2BE\3\2\2\2CA\3\2\2\2CD\3\2\2\2DF\3\2\2\2EC\3\2\2\2FG\7\17\2\2"+
		"G\t\3\2\2\2HI\7\4\2\2Il\7&\2\2JK\7\3\2\2Kl\7&\2\2LM\7\7\2\2MN\5\22\n\2"+
		"NO\7\b\2\2OP\5\24\13\2PQ\7\t\2\2Ql\3\2\2\2RS\7\n\2\2ST\5\22\n\2TU\7\b"+
		"\2\2UV\5\26\f\2VW\7\13\2\2Wl\3\2\2\2XY\7&\2\2YZ\7\36\2\2Zl\5\f\7\2[\\"+
		"\7&\2\2\\]\7\23\2\2]^\7\'\2\2^_\7\24\2\2_`\7\36\2\2`l\5\f\7\2ab\7&\2\2"+
		"bc\7\26\2\2cd\7&\2\2de\7\36\2\2el\5\f\7\2fg\7\20\2\2gh\7&\2\2hi\7\21\2"+
		"\2il\7\22\2\2jl\7*\2\2kH\3\2\2\2kJ\3\2\2\2kL\3\2\2\2kR\3\2\2\2kX\3\2\2"+
		"\2k[\3\2\2\2ka\3\2\2\2kf\3\2\2\2kj\3\2\2\2l\13\3\2\2\2mw\5\16\b\2no\5"+
		"\16\b\2op\7\31\2\2pq\5\16\b\2qw\3\2\2\2rs\5\16\b\2st\7\32\2\2tu\5\16\b"+
		"\2uw\3\2\2\2vm\3\2\2\2vn\3\2\2\2vr\3\2\2\2w\r\3\2\2\2x\u0082\5\20\t\2"+
		"yz\5\20\t\2z{\7\33\2\2{|\5\20\t\2|\u0082\3\2\2\2}~\5\20\t\2~\177\7\34"+
		"\2\2\177\u0080\5\20\t\2\u0080\u0082\3\2\2\2\u0081x\3\2\2\2\u0081y\3\2"+
		"\2\2\u0081}\3\2\2\2\u0082\17\3\2\2\2\u0083\u009e\7&\2\2\u0084\u009e\7"+
		"\'\2\2\u0085\u009e\7(\2\2\u0086\u0087\7\27\2\2\u0087\u009e\5\20\t\2\u0088"+
		"\u0089\7\30\2\2\u0089\u009e\5\20\t\2\u008a\u008b\7\21\2\2\u008b\u008c"+
		"\5\f\7\2\u008c\u008d\7\22\2\2\u008d\u009e\3\2\2\2\u008e\u009e\7%\2\2\u008f"+
		"\u0090\7\5\2\2\u0090\u0091\7\6\2\2\u0091\u0092\7\23\2\2\u0092\u0093\7"+
		"\'\2\2\u0093\u009e\7\24\2\2\u0094\u0095\7&\2\2\u0095\u0096\7\23\2\2\u0096"+
		"\u0097\7\'\2\2\u0097\u009e\7\24\2\2\u0098\u0099\7\16\2\2\u0099\u009e\7"+
		"&\2\2\u009a\u009b\7&\2\2\u009b\u009c\7\26\2\2\u009c\u009e\7&\2\2\u009d"+
		"\u0083\3\2\2\2\u009d\u0084\3\2\2\2\u009d\u0085\3\2\2\2\u009d\u0086\3\2"+
		"\2\2\u009d\u0088\3\2\2\2\u009d\u008a\3\2\2\2\u009d\u008e\3\2\2\2\u009d"+
		"\u008f\3\2\2\2\u009d\u0094\3\2\2\2\u009d\u0098\3\2\2\2\u009d\u009a\3\2"+
		"\2\2\u009e\21\3\2\2\2\u009f\u00a0\5\f\7\2\u00a0\u00a1\7\37\2\2\u00a1\u00a2"+
		"\5\f\7\2\u00a2\u00b8\3\2\2\2\u00a3\u00a4\5\f\7\2\u00a4\u00a5\7 \2\2\u00a5"+
		"\u00a6\5\f\7\2\u00a6\u00b8\3\2\2\2\u00a7\u00a8\5\f\7\2\u00a8\u00a9\7!"+
		"\2\2\u00a9\u00aa\5\f\7\2\u00aa\u00b8\3\2\2\2\u00ab\u00ac\5\f\7\2\u00ac"+
		"\u00ad\7\"\2\2\u00ad\u00ae\5\f\7\2\u00ae\u00b8\3\2\2\2\u00af\u00b0\5\f"+
		"\7\2\u00b0\u00b1\7#\2\2\u00b1\u00b2\5\f\7\2\u00b2\u00b8\3\2\2\2\u00b3"+
		"\u00b4\5\f\7\2\u00b4\u00b5\7$\2\2\u00b5\u00b6\5\f\7\2\u00b6\u00b8\3\2"+
		"\2\2\u00b7\u009f\3\2\2\2\u00b7\u00a3\3\2\2\2\u00b7\u00a7\3\2\2\2\u00b7"+
		"\u00ab\3\2\2\2\u00b7\u00af\3\2\2\2\u00b7\u00b3\3\2\2\2\u00b8\23\3\2\2"+
		"\2\u00b9\u00ba\5\4\3\2\u00ba\25\3\2\2\2\u00bb\u00bc\5\4\3\2\u00bc\27\3"+
		"\2\2\2\u00bd\u00be\5\4\3\2\u00be\31\3\2\2\2\u00bf\u00c0\7\6\2\2\u00c0"+
		"\u00c1\7\25\2\2\u00c1\u00c3\7&\2\2\u00c2\u00bf\3\2\2\2\u00c2\u00c3\3\2"+
		"\2\2\u00c3\u00c4\3\2\2\2\u00c4\u00c6\7\35\2\2\u00c5\u00c2\3\2\2\2\u00c6"+
		"\u00c7\3\2\2\2\u00c7\u00c5\3\2\2\2\u00c7\u00c8\3\2\2\2\u00c8\33\3\2\2"+
		"\2\17\37$(->Ckv\u0081\u009d\u00b7\u00c2\u00c7";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}