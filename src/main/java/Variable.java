public class Variable {
    private VariableType type;
    private String value;
    private Integer arraySize;

    public Variable(VariableType type, String value) {
        this.type = type;
        this.value = value;
        this.arraySize = null;
    }

    public Variable(VariableType type, Integer arraySize) {
        this.type = type;
        this.value = null;
        this.arraySize = arraySize;
    }

    public VariableType getType() {
        return type;
    }

    public String getValue() {
        return value;
    }

    public Integer getArraySize() {
        return arraySize;
    }
}
