grammar Superlang;

prog: ( (stat | functionDefinition | structDefinition )? NEWLINE )*
    ;

block: ( stat? NEWLINE)*
    ;

functionDefinition: FUNCTION ID COLON NEWLINE functionBlock ENDFUNCTION
    ;

structDefinition: STRUCT ID COLON NEWLINE ( (TYP COLON ID)? NEWLINE)* ENDSTRUCT
    ;

stat: WRITE ID                                                            #write
    | READ ID                                                             #read
    | IF condition THEN ifBlock ENDIF                                     #conditional
    | WHILE condition THEN loopBlock ENDWHILE                             #loop
    | ID IS_EQUAL expr1                                                   #assign
    | ID OPENING_SQUARE_BRACKET INT CLOSING_SQUARE_BRACKET IS_EQUAL expr1 #assignElementTablicy
    | ID DOT ID IS_EQUAL expr1                                            #assignElementStruktury
    | CALL ID OPENING_BRACKET CLOSING_BRACKET                             #callFunction
    | COMMENT                                                             #comment
    ;

expr1: expr2		   #singleExpr2
     | expr2 ADD expr2 #add
     | expr2 SUB expr2 #sub
    ;

expr2: exprBasic			    #singleExprBasic
     | exprBasic MULT exprBasic #mult
     | exprBasic DIV exprBasic  #div
    ;

exprBasic: ID                                                            #id
         | INT                                                           #int
         | REAL                                                          #real
         | TOINT exprBasic                                               #toInt
         | TOREAL exprBasic                                              #toReal
         | OPENING_BRACKET expr1 CLOSING_BRACKET                         #brackets
         | STRING                                                        #string
         | TABLICA TYP OPENING_SQUARE_BRACKET INT CLOSING_SQUARE_BRACKET #tablica
         | ID OPENING_SQUARE_BRACKET INT CLOSING_SQUARE_BRACKET          #elementTablicy
         | STRUCT ID                                                     #struktura
         | ID DOT ID                                                     #elementStruktury
    ;

condition: expr1 EQUAL expr1                 #equalCond
         | expr1 NOT_EQUAL expr1             #notEqualCond
         | expr1 LESSER_THAN expr1           #lessThanCond
         | expr1 LESSER_OR_EQUAL_THAN expr1  #lessOrEqualCond
         | expr1 GREATER_THAN expr1          #greaterThanCond
         | expr1 GREATER_OR_EQUAL_THAN expr1 #greaterOrEqualCond
         ;

ifBlock: block
    ;

loopBlock: block
    ;

functionBlock: block
    ;

structFields: ( (TYP COLON ID)? NEWLINE)+
    ;

READ: 'CZYTAJ'
    ;

WRITE: 'PISZ'
    ;

TABLICA: 'TAB'
    ;

TYP: 'CAL'
   | 'RZ'
   | 'NAP'
    ;

IF: 'JEZELI'
    ;

THEN: 'WTEDY'
    ;

ENDIF: 'KONJEZELI'
    ;

WHILE: 'PETLA'
    ;

ENDWHILE: 'KONPETLA'
    ;

FUNCTION: 'FUNKCJA'
    ;

ENDFUNCTION: 'KONFUNKCJA'
    ;

STRUCT: 'STRUKTURA'
    ;

ENDSTRUCT: 'KONSTRUKTURA'
    ;

CALL: 'WYWOLAJ'
    ;

OPENING_BRACKET: '('
    ;

CLOSING_BRACKET: ')'
    ;

OPENING_SQUARE_BRACKET: '['
    ;

CLOSING_SQUARE_BRACKET: ']'
    ;

COLON: ':'
    ;

DOT: '.'
    ;

TOINT: '(cal)'
    ;

TOREAL: '(rz)'
    ;

ADD: '+'
    ;

SUB: '-'
    ;

MULT: '*'
    ;

DIV: '/'
    ;

NEWLINE: '\r'? '\n'
    ;

IS_EQUAL: '='
    ;

EQUAL: '=='
    ;

NOT_EQUAL: '~='
    ;

LESSER_THAN: '<'
    ;

LESSER_OR_EQUAL_THAN : '<='
    ;

GREATER_THAN: '>'
    ;

GREATER_OR_EQUAL_THAN: '>='
    ;

STRING: '"' ( ~('\\'|'"') )* '"'
    ;

ID: ('a'..'z'|'A'..'Z')+
    ;

INT: '0'..'9'+
    ;

REAL: '0'..'9'+'.''0'..'9'+
    ;

WS: (' '|'\t')+ -> skip
    ;

COMMENT: '#' ~[\r\n]* -> skip
    ;
