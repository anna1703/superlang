import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import superlang.SuperlangLexer;
import superlang.SuperlangParser;

public class Superlang {
    public static void main(String... args) throws Exception {
//        File inputFile = new File(args[0]);
//        InputStream inputFileStream = new FileInputStream(inputFile.getAbsolutePath());
//        CharStream input = CharStreams.fromStream(inputFileStream);


        ANTLRFileStream input = new ANTLRFileStream(args[0]);

        SuperlangLexer lexer = new SuperlangLexer(input);

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        SuperlangParser parser = new SuperlangParser(tokens);
        ParseTree tree = parser.prog();

        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new LLVMActions(), tree);

    }
}
