public enum VariableType {
    INT("i32", "@strpi", "0"),
    REAL("double", "@strpd", "0.0"),
    STRING("i8*", "@strps", "null"),
    ARRAY_INT("[%d x i32]", "", "zeroinitializer"),
    ARRAY_REAL("[%d x double]", "", "zeroinitializer"),
    STRUCT("%struct.", "", "zeroinitializer"),
    UNKNOWN("", "", "");

    private final String type;
    private final String printfFormat;
    private final String initVal;

    VariableType(String type, String printfFormat, String initVal) {
        this.type = type;
        this.printfFormat = printfFormat;
        this.initVal = initVal;
    }

    public String getType() {
        return type;
    }

    public String getPrintfFormat() {
        return printfFormat;
    }

    public String getInitVal() {
        return initVal;
    }
}