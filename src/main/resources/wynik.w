declare i32 @printf(i8*, ...)
declare i32 @scanf(i8*, ...)
@strpi = constant [4 x i8] c"%d\0A\00"
@strpd = constant [4 x i8] c"%f\0A\00"
@strps = constant [4 x i8] c"%s\0A\00"
@strsi =  constant [3 x i8] c"%d\00"
@strsd = constant [4 x i8] c"%lf\00"
@strinit = constant [1 x i8] c"\00"
@str1 = constant[8 x i8] c"JEST OK\00"
define void @nowa() nounwind {
%liczba = alloca i32
store i32 12345, i32* %liczba
%1 = load i32* %liczba
%2 = icmp eq i32 %1, 12345
br i1 %2, label %ok3, label %fail3
ok3: 
%3 = load i32* %liczba
%4 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpi, i32 0, i32 0), i32 %3)
br label %fail3
fail3:
ret void
}
%struct.Strukt = type { i32, i32 }
@druga = common global [4 x double] zeroinitializer
@element = common global double 0.0
@variable = common global i32 0
@printZm = common global i8* null
@i = common global i32 0
@liczba = common global i32 0
@sss = common global %struct.Strukt zeroinitializer
@testtt = common global i32 0
@testt = common global i32 0
define i32 @main() nounwind{
%1 = getelementptr inbounds [4 x double]* @druga, i32 0, i32 0
store double 3.50, double* %1
%2 = load double* getelementptr inbounds ([4 x double]* @druga, i32 0, i32 0)
store double %2, double* @element
%3 = load double* @element
%4 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpd, i32 0, i32 0), double %3)
store i32 6, i32* @variable
%5 = load i32* @variable
%6 = icmp eq i32 %5, 6
br i1 %6, label %ok1, label %fail1
ok1: 
store i8* getelementptr inbounds ([8 x i8]* @str1, i32 0, i32 0), i8** @printZm
%7 = load i8** @printZm
%8 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strps, i32 0, i32 0), i8* %7)
br label %fail1
fail1:
store i32 1, i32* @i
store i32 123, i32* @liczba
br label %loop2Head
loop2Head:
%9 = load i32* @i
%10 = icmp slt i32 %9, 3
br i1 %10, label %loop2Body, label %loop2End
loop2Body:
%11 = load i32* @i
%12 = add i32 1, %11
store i32 %12, i32* @i
%13 = load i32* @i
%14 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpi, i32 0, i32 0), i32 %13)
br label %loop2Head
loop2End:
call void @nowa()
%15 = load i32* @liczba
%16 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpi, i32 0, i32 0), i32 %15)
%17 = getelementptr inbounds %struct.Strukt* @sss, i32 0, i32 1
store i32 567, i32* %17
%18 = getelementptr inbounds %struct.Strukt* @sss, i32 0, i32 0
store i32 766, i32* %18
%19 = load i32* getelementptr inbounds (%struct.Strukt* @sss, i32 0, i32 0)
store i32 %19, i32* @testtt
%20 = load i32* getelementptr inbounds (%struct.Strukt* @sss, i32 0, i32 1)
store i32 %20, i32* @testt
%21 = load i32* @testt
%22 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpi, i32 0, i32 0), i32 %21)
%23 = load i32* @testtt
%24 = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* @strpi, i32 0, i32 0), i32 %23)
ret i32 0 
}

