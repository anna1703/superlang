import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

class LLVMGenerator {

    static String HEADER_TEXT = "";
    static String MAIN_TEXT = "";
    static String BUFFER = "";
    static String GLOBALS = "";

    static int IF_OR_LOOP_NO = 0;
    static int REG = 1;
    static int STR_REG = 1;
    static int GLOBAL_REG_BACKUP = 1;

    static Stack<Integer> BRACE_STACK = new Stack<>();

    /** ************************************ **/
    /** ******* Generowanie całości ******** **/
    /** ************************************ **/

    static String generate() {
        return "declare i32 @printf(i8*, ...)\n"
                + "declare i32 @scanf(i8*, ...)\n"
                + "@strpi = constant [4 x i8] c\"%d\\0A\\00\"\n"
                + "@strpd = constant [4 x i8] c\"%f\\0A\\00\"\n"
                + "@strps = constant [4 x i8] c\"%s\\0A\\00\"\n"
                + "@strsi =  constant [3 x i8] c\"%d\\00\"\n"
                + "@strsd = constant [4 x i8] c\"%lf\\00\"\n"
                + "@strinit = constant [1 x i8] c\"\\00\"\n"
                + HEADER_TEXT
                + GLOBALS
                + "define i32 @main() nounwind{\n"
                + MAIN_TEXT
                + "ret i32 0 \n"
                + "}\n";
    }

    static void closeMain() {
        MAIN_TEXT += BUFFER;
    }

    /** ************************************ **/
    /** ******* Ładowanie, deklaracja ****** **/
    /** ************************************ **/

    static int load(VariableType varType, Scope scope, String id, Integer arraySize, Integer arrayIndex) {
        if (varType == VariableType.ARRAY_INT) {
            BUFFER += "%" + REG + " = load i32* getelementptr inbounds (" + String.format(varType.getType(), arraySize) + "* " + scope.getPrefix() + id + ", i32 0, i32 " + arrayIndex + ")\n";
        } else if (varType == VariableType.ARRAY_REAL) {
            BUFFER += "%" + REG + " = load double* getelementptr inbounds (" + String.format(varType.getType(), arraySize) + "* " + scope.getPrefix() + id + ", i32 0, i32 " + arrayIndex + ")\n";
        } else {
            BUFFER += "%" + REG + " = load " + varType.getType() + "* " + scope.getPrefix() + id + "\n";
        }
        return REG++;
    }

    static int loadFromStructure(Scope scope, String id, VariableType elemType, String structName, Integer elementIndex) {
        if (elemType == VariableType.INT) {
            BUFFER += "%" + REG + " = load i32* getelementptr inbounds (" + VariableType.STRUCT.getType() + structName + "* " + scope.getPrefix() + id + ", i32 0, i32 " + elementIndex + ")\n";
        } else if (elemType == VariableType.REAL) {
            BUFFER += "%" + REG + " = load double* getelementptr inbounds (" + VariableType.STRUCT.getType() + structName + "* " + scope.getPrefix() + id + ", i32 0, i32 " + elementIndex + ")\n";
        }
        return REG++;
    }

    static void declare(Scope scope, String id, VariableType type, Variable variable) {
        if (scope == Scope.LOCAL) {
            BUFFER += scope.getPrefix() + id + " = alloca " + type.getType() + "\n";
        } else if (scope == Scope.GLOBAL) {
            if (type == VariableType.ARRAY_INT || type == VariableType.ARRAY_REAL) {
                GLOBALS += scope.getPrefix() + id + " = common global " + String.format(type.getType(), variable.getArraySize()) + " " + type.getInitVal() + "\n";
            } else if (type == VariableType.STRUCT) {
                GLOBALS += scope.getPrefix() + id + " = common global " + type.getType() + variable.getValue() + " " + type.getInitVal() + "\n";
            } else {
                GLOBALS += scope.getPrefix() + id + " = common global " + type.getType() + " " + type.getInitVal() + "\n";
            }
        }
    }

    /** ************************************ **/
    /** ******** Pisanie, czytanie ********* **/
    /** ************************************ **/

    static void print(VariableType varType, Scope scope, String id) {
        load(varType, scope, id, null, null);
        BUFFER += "%" + REG + " = call i32 (i8*, ...)* @printf(i8* getelementptr inbounds ([4 x i8]* " + varType.getPrintfFormat() + ", i32 0, i32 0), " + varType.getType() + " %" + (REG - 1) + ")\n";
        REG++;
    }

    static void scanInt(Scope scope, String id) {
        BUFFER += "%" + REG + " = call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds ([3 x i8]* @strsi, i32 0, i32 0), i32* " + scope.getPrefix() + id + ")\n";
        REG++;
    }

    static void scanReal(Scope scope, String id) {
        BUFFER += "%" + REG + " = call i32 (i8*, ...)* @scanf(i8* getelementptr inbounds ([4 x i8]* @strsd, i32 0, i32 0), double* " + scope.getPrefix() + id + ")\n";
        REG++;
    }

    /** ************************************ **/
    /** ********** Przypisywanie *********** **/
    /** ************************************ **/

    static void assignInt(Scope scope, String id, String value) {
        BUFFER += "store i32 " + value + ", i32* " + scope.getPrefix() + id + "\n";
    }

    static void assignReal(Scope scope, String id, String value) {
        BUFFER += "store double " + value + ", double* " + scope.getPrefix() + id + "\n";
    }

    static void assignString(Scope scope, String id, String value) {
        HEADER_TEXT += "@str" + STR_REG + " = constant[" + (value.length() + 1) + " x i8] c\"" + value + "\\00\"\n";
        BUFFER += "store i8* getelementptr inbounds ([" + (value.length() + 1) + " x i8]* @str" + STR_REG + ", i32 0, i32 0), i8** " + scope.getPrefix() + id + "\n";
        STR_REG++;
    }

    static void assignArrayIntElement(Scope scope, String id, String value, Integer arraySize, Integer arrayIndex) {
        BUFFER += "%" + REG + " = getelementptr inbounds " + String.format(VariableType.ARRAY_INT.getType(), arraySize) + "* " + scope.getPrefix() + id + ", i32 0, i32 " + arrayIndex + "\n";
        BUFFER += "store i32 " + value + ", i32* %" + REG + "\n";
        REG++;
    }

    static void assignArrayRealElement(Scope scope, String id, String value, Integer arraySize, Integer arrayIndex) {
        BUFFER += "%" + REG + " = getelementptr inbounds " + String.format(VariableType.ARRAY_REAL.getType(), arraySize) + "* " + scope.getPrefix() + id + ", i32 0, i32 " + arrayIndex + "\n";
        BUFFER += "store double " + value + ", double* %" + REG + "\n";
        REG++;
    }

    /** ************************************ **/
    /** ************ Rzutowanie ************ **/
    /** ************************************ **/

    static String realToInt(String id) {
        BUFFER += "%" + REG + " = fptosi double " + id + " to i32\n";
        REG++;
        return "%" + (REG - 1);
    }

    static String intToReal(String id) {
        BUFFER += "%" + REG + " = sitofp i32 " + id + " to double\n";
        REG++;
        return "%" + (REG - 1);
    }

    /** ************************************ **/
    /** ****** Działania arytmetyczne ****** **/
    /** ************************************ **/

    static void addInt(String var1, String var2) {
        BUFFER += "%" + REG + " = add i32 " + var1 + ", " + var2 + "\n";
        REG++;
    }

    static void addReal(String var1, String var2) {
        BUFFER += "%" + REG + " = fadd double " + var1 + ", " + var2 + "\n";
        REG++;
    }

    static void subInt(String var1, String var2) {
        BUFFER += "%" + REG + " = sub i32 " + var2 + ", " + var1 + "\n";
        REG++;
    }

    static void subReal(String var1, String var2) {
        BUFFER += "%" + REG + " = fsub double " + var2 + ", " + var1 + "\n";
        REG++;
    }

    static void multInt(String var1, String var2) {
        BUFFER += "%" + REG + " = mul i32 " + var1 + ", " + var2 + "\n";
        REG++;
    }

    static void multReal(String var1, String var2) {
        BUFFER += "%" + REG + " = fmul double " + var1 + ", " + var2 + "\n";
        REG++;
    }

    static void divInt(String var1, String var2) {
        BUFFER += "%" + REG + " = sdiv i32 " + var2 + ", " + var1 + "\n";
        REG++;
    }

    static void divReal(String var1, String var2) {
        BUFFER += "%" + REG + " = fdiv double " + var2 + ", " + var1 + "\n";
        REG++;
    }

    /** ************************************ **/
    /** ************* Warunki ************** **/
    /** ************************************ **/

    static int icmp(String id1, String id2, ConditionType condType, VariableType varType) {
        BUFFER += "%" + REG + " = icmp " + condType.getValue() + " " + varType.getType() + " " + id1 + ", " + id2 + "\n";
        return REG++;
    }

    /** ************************************ **/
    /** ****** Instrukcje warunkowe ******** **/
    /** ************************************ **/

    static void startIf(String cond) {
        IF_OR_LOOP_NO++;
        BUFFER += "br i1 " + cond + ", label %ok" + IF_OR_LOOP_NO + ", label %fail" + IF_OR_LOOP_NO + "\n";
        BUFFER += "ok" + IF_OR_LOOP_NO + ": \n";
        BRACE_STACK.push(IF_OR_LOOP_NO);
    }

    static void endIf() {
        int brNo = BRACE_STACK.pop();
        BUFFER += "br label %fail" + brNo + "\n";
        BUFFER += "fail" + brNo + ":\n";
    }

    /** ************************************ **/
    /** ************** Pętle *************** **/
    /** ************************************ **/

    static void loopHead() {
        IF_OR_LOOP_NO++;
        BRACE_STACK.push(IF_OR_LOOP_NO);
        BUFFER += "br label %loop" + IF_OR_LOOP_NO + "Head\n";
        BUFFER += "loop" + IF_OR_LOOP_NO + "Head:\n";
    }

    static void startLoop(String cond) {
        BUFFER += "br i1 " + cond + ", label %loop" + IF_OR_LOOP_NO + "Body, label %loop" + IF_OR_LOOP_NO + "End\n";
        BUFFER += "loop" + IF_OR_LOOP_NO + "Body:\n";
    }

    static void endLoop() {
        int brNo = BRACE_STACK.pop();
        BUFFER += "br label %loop" + brNo + "Head\n";
        BUFFER += "loop" + brNo + "End:\n";
    }

    /** ************************************ **/
    /** ************* Funkcje ************** **/
    /** ************************************ **/

    static void startFunction(String id) {
        MAIN_TEXT += BUFFER;
        GLOBAL_REG_BACKUP = REG;
        BUFFER = "define void @" + id + "() nounwind {\n";
        REG = 1;
    }

    static void endFunction() {
        BUFFER += "ret void\n}\n";
        HEADER_TEXT += BUFFER;
        BUFFER = "";
        REG = GLOBAL_REG_BACKUP;
    }

    static void callFunction(String id) {
        BUFFER += "call void @" + id + "()\n";
    }

    /** ************************************ **/
    /** ************ Struktury ************* **/
    /** ************************************ **/

    static void structure(String id, List<VariableType> types) {
        String typesList = String.join(", ", types.stream().map(VariableType::getType).collect(Collectors.toList()));
        HEADER_TEXT += "%struct." + id + " = type { " + typesList + " }\n";
    }

    static void assignStructureIntElement(Scope scope, String id, String value, String structName, Integer elementIndex) {
        BUFFER += "%" + REG + " = getelementptr inbounds " + VariableType.STRUCT.getType() + structName + "* " + scope.getPrefix() + id + ", i32 0, i32 " + elementIndex + "\n";
        BUFFER += "store i32 " + value + ", i32* %" + REG + "\n";
        REG++;
    }

    static void assignStructureRealElement(Scope scope, String id, String value, String structName, Integer elementIndex) {
        BUFFER += "%" + REG + " = getelementptr inbounds " + VariableType.STRUCT.getType() + structName + "* " + scope.getPrefix() + id + ", i32 0, i32 " + elementIndex + "\n";
        BUFFER += "store double " + value + ", double* %" + REG + "\n";
        REG++;
    }

}
