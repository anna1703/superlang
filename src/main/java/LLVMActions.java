import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import superlang.SuperlangBaseListener;
import superlang.SuperlangParser;

import java.util.*;
import java.util.stream.Collectors;

public class LLVMActions extends SuperlangBaseListener {

    private Map<String, Variable> globalVariables = new HashMap<>();
    private Map<String, Variable> localVariables = new HashMap<>();

    private Stack<Variable> variables = new Stack<>();
    private Stack<String> conditions = new Stack<>();
    private Set<String> functions = new HashSet<>();
    private Set<Structure> structures = new HashSet<>();

    private boolean global;

    @Override
    public void enterProg(SuperlangParser.ProgContext ctx) {
        setGlobalScope();
    }

    @Override
    public void exitProg(SuperlangParser.ProgContext ctx) {
        LLVMGenerator.closeMain();
        System.out.println(LLVMGenerator.generate());
    }

    /** ************************************ **/
    /** ** Obsługa zmiennych i scope'ów **** **/
    /** ************************************ **/

    private VariableType getVariableType(String varName) {
        Map<String, Variable> variables;
        variables = getScopeVariables();
        Variable var = variables.get(varName);
        return var != null ? var.getType() : VariableType.UNKNOWN;
    }

    private Map<String, Variable> getScopeVariables() {
        return global ? globalVariables : localVariables;
    }

    private Scope getScopeOfVariable(String id) {
        if (localVariables.containsKey(id)) {
            return Scope.LOCAL;
        } else if (globalVariables.containsKey(id)) {
            return Scope.GLOBAL;
        }
        return null;
    }

    private Scope getCurrentScope() {
        return global ? Scope.GLOBAL : Scope.LOCAL;
    }

    private void setGlobalScope() {
        global = true;
        localVariables.clear();
    }

    private void setLocalScope() {
        global = false;
        localVariables.clear();
    }

    /** ************************************ **/
    /** ******** Pisanie, czytanie ********* **/
    /** ************************************ **/

    @Override
    public void exitRead(SuperlangParser.ReadContext ctx) {
        String id = ctx.ID().getText();
        VariableType varType = getVariableType(id);
        Scope scope = getScopeOfVariable(id);

        switch (varType) {
            case INT:
                LLVMGenerator.scanInt(scope, id);
                break;
            case REAL:
                LLVMGenerator.scanReal(scope, id);
                break;
            case UNKNOWN:
                declareVariable(id, VariableType.REAL, null);
                LLVMGenerator.scanReal(getCurrentScope(), id);
                break;
            case STRING:
                throw new RuntimeException(String.format("Linia %d: nie można wczytywać innych zmiennych niż typu int i real",
                        ctx.getStart().getLine()));
        }
    }

    @Override
    public void exitWrite(SuperlangParser.WriteContext ctx) {
        String id = ctx.ID().getText();
        VariableType varType = getVariableType(id);
        if (varType == VariableType.UNKNOWN) {
            throw new RuntimeException(String.format("Linia %d: Nieznana zmienna %s", ctx.getStart().getLine(), id));
        } else {
            LLVMGenerator.print(varType, getScopeOfVariable(id), id);
        }
    }

    /** ************************************ **/
    /** *** Przypisywanie, deklarowanie **** **/
    /** ************************************ **/

    @Override
    public void exitId(SuperlangParser.IdContext ctx) {
        String id = ctx.ID().getText();
        VariableType varType = getVariableType(id);
        if (varType == VariableType.UNKNOWN) {
            throw new RuntimeException(String.format("Linia %d: Nieznana zmienna %s", ctx.getStart().getLine(), id));
        } else {
            int reg = LLVMGenerator.load(varType, getScopeOfVariable(id), id, null, null);
            variables.push(new Variable(varType, "%" + reg));
        }
    }

    @Override
    public void exitAssign(SuperlangParser.AssignContext ctx) {
        String id = ctx.ID().getText();
        Variable var = variables.pop();
        if (var.getType() == VariableType.UNKNOWN) {
            throw new RuntimeException(String.format("Linia %d: Nieznany typ", ctx.getStart().getLine()));
        }
        if (getVariableType(id).equals(VariableType.UNKNOWN)) {
            declareVariable(id, var.getType(), var);
            assign(id, var);
        } else if (getVariableType(id).equals(var.getType())) {
            assign(id, var);
        } else {
            throw new RuntimeException(String.format("Linia %d: Niezgodność wartości i typu zmiennej", ctx.getStart().getLine()));
        }
    }

    @Override
    public void exitAssignElementTablicy(SuperlangParser.AssignElementTablicyContext ctx) {
        String id = ctx.ID().getText();
        Integer intVal = Integer.parseInt(ctx.INT().getText());
        Variable var = variables.pop();
        if (var.getType() == VariableType.UNKNOWN) {
            throw new RuntimeException(String.format("Linia %d: Nieznany typ", ctx.getStart().getLine()));
        }
        if (getVariableType(id).equals(VariableType.ARRAY_INT)) {
            Variable arrayVar = getScopeVariables().get(id);
            LLVMGenerator.assignArrayIntElement(getScopeOfVariable(id), id, var.getValue(), arrayVar.getArraySize(), intVal);
        } else if (getVariableType(id).equals(VariableType.ARRAY_REAL)) {
            Variable arrayVar = getScopeVariables().get(id);
            LLVMGenerator.assignArrayRealElement(getScopeOfVariable(id), id, var.getValue(), arrayVar.getArraySize(), intVal);
        } else {
            throw new RuntimeException("");
        }
    }

    private void assign(String id, Variable v) {
        switch (v.getType()) {
            case INT:
                LLVMGenerator.assignInt(getScopeOfVariable(id), id, v.getValue());
                break;
            case REAL:
                LLVMGenerator.assignReal(getScopeOfVariable(id), id, v.getValue());
                break;
            case STRING:
                LLVMGenerator.assignString(getScopeOfVariable(id), id, v.getValue());
                break;
        }
    }

    private void declareVariable(String id, VariableType variableType, Variable variable) {
        Variable var = (variable == null ? new Variable(variableType, id) : variable);
        getScopeVariables().put(id, var);
        LLVMGenerator.declare(getCurrentScope(), id, variableType, variable);
    }

    /** ************************************ **/
    /** ********* Podstawowe typy ********** **/
    /** ************************************ **/

    @Override
    public void exitInt(SuperlangParser.IntContext ctx) {
        variables.push(new Variable(VariableType.INT, ctx.INT().getText()));
    }

    @Override
    public void exitReal(SuperlangParser.RealContext ctx) {
        variables.push(new Variable(VariableType.REAL, ctx.REAL().getText()));
    }

    @Override
    public void exitString(SuperlangParser.StringContext ctx) {
        String tmp = ctx.STRING().getText();
        String text = tmp.substring(1, tmp.length() - 1);
        variables.push(new Variable(VariableType.STRING, text));
    }

    @Override
    public void exitTablica(SuperlangParser.TablicaContext ctx) {
        String typ = ctx.TYP().getText();
        if (typ.equals("NAP")) {
            throw new RuntimeException("");
        }
        VariableType varType = typ.equals("CAL") ? VariableType.ARRAY_INT : VariableType.ARRAY_REAL;
        Integer arraySize = Integer.parseInt(ctx.INT().getText());
        variables.push(new Variable(varType, arraySize));
    }

    @Override
    public void exitElementTablicy(SuperlangParser.ElementTablicyContext ctx) {
        String id = ctx.ID().getText();
        Integer ind = Integer.parseInt(ctx.INT().getText());
        VariableType varType = getVariableType(id);
        if (varType == VariableType.UNKNOWN) {
            throw new RuntimeException(String.format("Linia %d: Nieznana zmienna %s", ctx.getStart().getLine(), id));
        } else if (varType == VariableType.ARRAY_INT) {
            int reg = LLVMGenerator.load(varType, getScopeOfVariable(id), id, getScopeVariables().get(id).getArraySize(), ind);
            variables.push(new Variable(VariableType.INT, "%" + reg));
        } else if (varType == VariableType.ARRAY_REAL) {
            int reg = LLVMGenerator.load(varType, getScopeOfVariable(id), id, getScopeVariables().get(id).getArraySize(), ind);
            variables.push(new Variable(VariableType.REAL, "%" + reg));
        }
    }

    @Override
    public void exitElementStruktury(SuperlangParser.ElementStrukturyContext ctx) {
        String idStruktury = ctx.ID(0).getText();
        String idPola = ctx.ID(1).getText();

        String structName = getScopeVariables().get(idStruktury).getValue();
        Structure structure = findStructureByName(structName, ctx.getStart().getLine());
        VariableType elemType = structure.getPropertyType(idPola, ctx.getStart().getLine());

        int reg = LLVMGenerator.loadFromStructure(getScopeOfVariable(idStruktury), idStruktury, elemType, structName, structure.getIds().indexOf(idPola));
        variables.push(new Variable(elemType, "%" + reg));
    }

    /** ************************************ **/
    /** ************ Rzutowanie ************ **/
    /** ************************************ **/

    @Override
    public void exitToInt(SuperlangParser.ToIntContext ctx) {
        Variable var = variables.pop();
        String value = LLVMGenerator.realToInt(var.getValue());
        variables.push(new Variable(VariableType.INT, value));
    }

    @Override
    public void exitToReal(SuperlangParser.ToRealContext ctx) {
        Variable var = variables.pop();
        String value = LLVMGenerator.intToReal(var.getValue());
        variables.push(new Variable(VariableType.REAL, value));
    }

    /** ************************************ **/
    /** ****** Działania arytmetyczne ****** **/
    /** ************************************ **/

    @Override
    public void exitAdd(SuperlangParser.AddContext ctx) {
        arithmeticOperation(LLVMGenerator::addInt, LLVMGenerator::addReal, "");
    }

    @Override
    public void exitSub(SuperlangParser.SubContext ctx) {
        arithmeticOperation(LLVMGenerator::subInt, LLVMGenerator::subReal, "");
    }

    @Override
    public void exitMult(SuperlangParser.MultContext ctx) {
        arithmeticOperation(LLVMGenerator::multInt, LLVMGenerator::multReal, "");
    }

    @Override
    public void exitDiv(SuperlangParser.DivContext ctx) {
        arithmeticOperation(LLVMGenerator::divInt, LLVMGenerator::divReal, "");
    }

    private void arithmeticOperation(ArithmeticOperation intOperation, ArithmeticOperation realOperation, String errorMsg) {
        Variable var1 = variables.pop();
        Variable var2 = variables.pop();
        if (var1.getType() == var2.getType()) {
            if (var1.getType() == VariableType.INT) {
                intOperation.doOperation(var1.getValue(), var2.getValue());
                variables.push(new Variable(VariableType.INT, "%" + (LLVMGenerator.REG - 1)));
            } else if (var1.getType() == VariableType.REAL) {
                realOperation.doOperation(var1.getValue(), var2.getValue());
                variables.push(new Variable(VariableType.REAL,"%" + (LLVMGenerator.REG - 1)));
            }
        } else {
            throw new RuntimeException("Niezgodne typy");
        }
    }

    @FunctionalInterface
    public interface ArithmeticOperation {
        void doOperation(String var1, String var2);
    }

    /** ************************************ **/
    /** ************ Warunki *************** **/
    /** ************************************ **/

    @Override
    public void exitEqualCond(SuperlangParser.EqualCondContext ctx) {
        compare(ctx, ConditionType.EQUAL);
    }

    @Override
    public void exitNotEqualCond(SuperlangParser.NotEqualCondContext ctx) {
        compare(ctx, ConditionType.NOT_EQUAL);
    }

    @Override
    public void exitGreaterThanCond(SuperlangParser.GreaterThanCondContext ctx) {
        compare(ctx, ConditionType.GREATER_THAN);
    }

    @Override
    public void exitGreaterOrEqualCond(SuperlangParser.GreaterOrEqualCondContext ctx) {
        compare(ctx, ConditionType.GREATER_OR_EQUAL);
    }

    @Override
    public void exitLessThanCond(SuperlangParser.LessThanCondContext ctx) {
        compare(ctx, ConditionType.LESS_THAN);
    }

    @Override
    public void exitLessOrEqualCond(SuperlangParser.LessOrEqualCondContext ctx) {
        compare(ctx, ConditionType.LESS_OR_EQUAL);
    }

    private void compare(ParserRuleContext ctx, ConditionType conditionType) {
        Variable var2 = variables.pop();
        Variable var1 = variables.pop();
        if (var1.getType() == var2.getType()) {
            if (var1.getType() == VariableType.INT) {
                int reg = LLVMGenerator.icmp(var1.getValue(), var2.getValue(), conditionType, var1.getType());
                conditions.push("%" + reg);
            } else {
                throw new RuntimeException("not implemented");
            }
        } else {
            throw new RuntimeException(
                    String.format("Linia %d: Porównywane zmienne muszą być tego samego typu", ctx.getStart().getLine()));
        }
    }

    /** ************************************ **/
    /** ****** Instrukcje warunkowe ******** **/
    /** ************************************ **/

    @Override
    public void enterIfBlock(SuperlangParser.IfBlockContext ctx) {
        LLVMGenerator.startIf(conditions.pop());
    }

    @Override
    public void exitIfBlock(SuperlangParser.IfBlockContext ctx) {
        LLVMGenerator.endIf();
    }

    /** ************************************ **/
    /** ************** Pętle *************** **/
    /** ************************************ **/

    @Override
    public void enterLoop(SuperlangParser.LoopContext ctx) {
        LLVMGenerator.loopHead();
    }

    @Override
    public void enterLoopBlock(SuperlangParser.LoopBlockContext ctx) {
        LLVMGenerator.startLoop(conditions.pop());
    }

    @Override
    public void exitLoopBlock(SuperlangParser.LoopBlockContext ctx) {
        LLVMGenerator.endLoop();
    }

    /** ************************************ **/
    /** ************* Funkcje ************** **/
    /** ************************************ **/

    @Override
    public void enterFunctionDefinition(SuperlangParser.FunctionDefinitionContext ctx) {
        String id = ctx.ID().getText();
        if (functions.contains(id)) {
            throw new RuntimeException(String.format("Linia %d: Funkcja %s już istnieje", ctx.getStart().getLine(), id));
        }
        setLocalScope();
        functions.add(id);
        LLVMGenerator.startFunction(id);
    }

    @Override
    public void exitFunctionDefinition(SuperlangParser.FunctionDefinitionContext ctx) {
        setGlobalScope();
        LLVMGenerator.endFunction();
    }

    @Override
    public void exitCallFunction(SuperlangParser.CallFunctionContext ctx) {
        String id = ctx.ID().getText();
        if (functions.contains(id)) {
            LLVMGenerator.callFunction(id);
        } else {
            throw new IllegalArgumentException(String.format("Linia %d: Funkcja %s nie istnieje", ctx.getStart().getLine(), id));
        }
    }

    /** ************************************ **/
    /** ************ Struktury ************* **/
    /** ************************************ **/

    private boolean doesStructureExist(String name) {
        return structures
                .stream()
                .anyMatch(s -> name.equals(s.getName()));
    }

    private Structure findStructureByName(String name, int lineNo) {
        return structures
                .stream()
                .filter(s -> name.equals(s.getName()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException(String.format("Linia %d: Struktura %s nie istnieje", lineNo, name)));
    }

    @Override
    public void exitStructDefinition(SuperlangParser.StructDefinitionContext ctx) {
        String id = ctx.ID(0).getText();
        if (doesStructureExist(id)) {
            throw new RuntimeException(String.format("Linia %d: Struktura %s już istnieje", ctx.getStart().getLine(), id));
        }
        List<String> ids = ctx.ID().stream().map(ParseTree::getText).collect(Collectors.toList());
        ids.remove(0);
        List<VariableType> types = ctx.TYP().stream().map(t -> {
            if (t.getText().equals("CAL")) {
                return VariableType.INT;
            } else if (t.getText().equals("RZ")) {
                return VariableType.REAL;
            } else {
                throw new RuntimeException(String.format("Linia %d: Struktury nie obsługują typu %s", ctx.getStart().getLine(), t));
            }
        }).collect(Collectors.toList());
        structures.add(new Structure(id, types, ids));
        LLVMGenerator.structure(id, types);
    }

    @Override
    public void exitStruktura(SuperlangParser.StrukturaContext ctx) {
        String structName = ctx.ID().getText();
        variables.push(new Variable(VariableType.STRUCT, structName));
    }

    @Override
    public void exitAssignElementStruktury(SuperlangParser.AssignElementStrukturyContext ctx) {
        String idStruktury = ctx.ID(0).getText();
        String idPola = ctx.ID(1).getText();
        Variable var = variables.pop();

        String structName = getScopeVariables().get(idStruktury).getValue();
        Structure structure = findStructureByName(structName, ctx.getStart().getLine());
        VariableType expectedVarType = structure.getPropertyType(idPola, ctx.getStart().getLine());
        if (var.getType() != expectedVarType) {
            throw new RuntimeException(String.format("Linia %d: Typ inny niż oczekiwany", ctx.getStart().getLine()));
        }
        if (expectedVarType == VariableType.INT) {
            LLVMGenerator.assignStructureIntElement(getScopeOfVariable(idStruktury), idStruktury, var.getValue(), structName, structure.getIds().indexOf(idPola));
        } else if (expectedVarType == VariableType.REAL) {
            LLVMGenerator.assignStructureRealElement(getScopeOfVariable(idStruktury), idStruktury, var.getValue(), structName, structure.getIds().indexOf(idPola));
        } else {
            throw new RuntimeException("");
        }
    }
}
