public enum Scope {
    LOCAL("%"),
    GLOBAL("@");

    private String prefix;

    Scope(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }
}
