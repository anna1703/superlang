import java.util.List;

public class Structure {

    private String name;
    private List<VariableType> types;
    private List<String> ids;

    public Structure(String name, List<VariableType> types, List<String> ids) {
        this.name = name;
        this.types = types;
        this.ids = ids;
    }

    public String getName() {
        return name;
    }

    public List<VariableType> getTypes() {
        return types;
    }

    public List<String> getIds() {
        return ids;
    }

    public VariableType getPropertyType(String id, int lineNo) {
        int ind = ids.indexOf(id);
        if (ind < 0) {
            throw new RuntimeException(String.format("Linia %d: Pole %s struktury nie istnieje", lineNo, id));
        }
        return types.get(ind);
    }
}
