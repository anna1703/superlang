// Generated from superlang/Superlang.g4 by ANTLR 4.7.1
package superlang;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SuperlangParser}.
 */
public interface SuperlangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(SuperlangParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(SuperlangParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#block}.
	 * @param ctx the parse tree
	 */
	void enterBlock(SuperlangParser.BlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#block}.
	 * @param ctx the parse tree
	 */
	void exitBlock(SuperlangParser.BlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void enterFunctionDefinition(SuperlangParser.FunctionDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#functionDefinition}.
	 * @param ctx the parse tree
	 */
	void exitFunctionDefinition(SuperlangParser.FunctionDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#structDefinition}.
	 * @param ctx the parse tree
	 */
	void enterStructDefinition(SuperlangParser.StructDefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#structDefinition}.
	 * @param ctx the parse tree
	 */
	void exitStructDefinition(SuperlangParser.StructDefinitionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code write}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterWrite(SuperlangParser.WriteContext ctx);
	/**
	 * Exit a parse tree produced by the {@code write}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitWrite(SuperlangParser.WriteContext ctx);
	/**
	 * Enter a parse tree produced by the {@code read}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterRead(SuperlangParser.ReadContext ctx);
	/**
	 * Exit a parse tree produced by the {@code read}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitRead(SuperlangParser.ReadContext ctx);
	/**
	 * Enter a parse tree produced by the {@code conditional}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterConditional(SuperlangParser.ConditionalContext ctx);
	/**
	 * Exit a parse tree produced by the {@code conditional}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitConditional(SuperlangParser.ConditionalContext ctx);
	/**
	 * Enter a parse tree produced by the {@code loop}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterLoop(SuperlangParser.LoopContext ctx);
	/**
	 * Exit a parse tree produced by the {@code loop}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitLoop(SuperlangParser.LoopContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assign}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterAssign(SuperlangParser.AssignContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assign}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitAssign(SuperlangParser.AssignContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignElementTablicy}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterAssignElementTablicy(SuperlangParser.AssignElementTablicyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignElementTablicy}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitAssignElementTablicy(SuperlangParser.AssignElementTablicyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code assignElementStruktury}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterAssignElementStruktury(SuperlangParser.AssignElementStrukturyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code assignElementStruktury}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitAssignElementStruktury(SuperlangParser.AssignElementStrukturyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code callFunction}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterCallFunction(SuperlangParser.CallFunctionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code callFunction}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitCallFunction(SuperlangParser.CallFunctionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code comment}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterComment(SuperlangParser.CommentContext ctx);
	/**
	 * Exit a parse tree produced by the {@code comment}
	 * labeled alternative in {@link SuperlangParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitComment(SuperlangParser.CommentContext ctx);
	/**
	 * Enter a parse tree produced by the {@code singleExpr2}
	 * labeled alternative in {@link SuperlangParser#expr1}.
	 * @param ctx the parse tree
	 */
	void enterSingleExpr2(SuperlangParser.SingleExpr2Context ctx);
	/**
	 * Exit a parse tree produced by the {@code singleExpr2}
	 * labeled alternative in {@link SuperlangParser#expr1}.
	 * @param ctx the parse tree
	 */
	void exitSingleExpr2(SuperlangParser.SingleExpr2Context ctx);
	/**
	 * Enter a parse tree produced by the {@code add}
	 * labeled alternative in {@link SuperlangParser#expr1}.
	 * @param ctx the parse tree
	 */
	void enterAdd(SuperlangParser.AddContext ctx);
	/**
	 * Exit a parse tree produced by the {@code add}
	 * labeled alternative in {@link SuperlangParser#expr1}.
	 * @param ctx the parse tree
	 */
	void exitAdd(SuperlangParser.AddContext ctx);
	/**
	 * Enter a parse tree produced by the {@code sub}
	 * labeled alternative in {@link SuperlangParser#expr1}.
	 * @param ctx the parse tree
	 */
	void enterSub(SuperlangParser.SubContext ctx);
	/**
	 * Exit a parse tree produced by the {@code sub}
	 * labeled alternative in {@link SuperlangParser#expr1}.
	 * @param ctx the parse tree
	 */
	void exitSub(SuperlangParser.SubContext ctx);
	/**
	 * Enter a parse tree produced by the {@code singleExprBasic}
	 * labeled alternative in {@link SuperlangParser#expr2}.
	 * @param ctx the parse tree
	 */
	void enterSingleExprBasic(SuperlangParser.SingleExprBasicContext ctx);
	/**
	 * Exit a parse tree produced by the {@code singleExprBasic}
	 * labeled alternative in {@link SuperlangParser#expr2}.
	 * @param ctx the parse tree
	 */
	void exitSingleExprBasic(SuperlangParser.SingleExprBasicContext ctx);
	/**
	 * Enter a parse tree produced by the {@code mult}
	 * labeled alternative in {@link SuperlangParser#expr2}.
	 * @param ctx the parse tree
	 */
	void enterMult(SuperlangParser.MultContext ctx);
	/**
	 * Exit a parse tree produced by the {@code mult}
	 * labeled alternative in {@link SuperlangParser#expr2}.
	 * @param ctx the parse tree
	 */
	void exitMult(SuperlangParser.MultContext ctx);
	/**
	 * Enter a parse tree produced by the {@code div}
	 * labeled alternative in {@link SuperlangParser#expr2}.
	 * @param ctx the parse tree
	 */
	void enterDiv(SuperlangParser.DivContext ctx);
	/**
	 * Exit a parse tree produced by the {@code div}
	 * labeled alternative in {@link SuperlangParser#expr2}.
	 * @param ctx the parse tree
	 */
	void exitDiv(SuperlangParser.DivContext ctx);
	/**
	 * Enter a parse tree produced by the {@code id}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterId(SuperlangParser.IdContext ctx);
	/**
	 * Exit a parse tree produced by the {@code id}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitId(SuperlangParser.IdContext ctx);
	/**
	 * Enter a parse tree produced by the {@code int}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterInt(SuperlangParser.IntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code int}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitInt(SuperlangParser.IntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code real}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterReal(SuperlangParser.RealContext ctx);
	/**
	 * Exit a parse tree produced by the {@code real}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitReal(SuperlangParser.RealContext ctx);
	/**
	 * Enter a parse tree produced by the {@code toInt}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterToInt(SuperlangParser.ToIntContext ctx);
	/**
	 * Exit a parse tree produced by the {@code toInt}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitToInt(SuperlangParser.ToIntContext ctx);
	/**
	 * Enter a parse tree produced by the {@code toReal}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterToReal(SuperlangParser.ToRealContext ctx);
	/**
	 * Exit a parse tree produced by the {@code toReal}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitToReal(SuperlangParser.ToRealContext ctx);
	/**
	 * Enter a parse tree produced by the {@code brackets}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterBrackets(SuperlangParser.BracketsContext ctx);
	/**
	 * Exit a parse tree produced by the {@code brackets}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitBrackets(SuperlangParser.BracketsContext ctx);
	/**
	 * Enter a parse tree produced by the {@code string}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterString(SuperlangParser.StringContext ctx);
	/**
	 * Exit a parse tree produced by the {@code string}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitString(SuperlangParser.StringContext ctx);
	/**
	 * Enter a parse tree produced by the {@code tablica}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterTablica(SuperlangParser.TablicaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code tablica}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitTablica(SuperlangParser.TablicaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code elementTablicy}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterElementTablicy(SuperlangParser.ElementTablicyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code elementTablicy}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitElementTablicy(SuperlangParser.ElementTablicyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code struktura}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterStruktura(SuperlangParser.StrukturaContext ctx);
	/**
	 * Exit a parse tree produced by the {@code struktura}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitStruktura(SuperlangParser.StrukturaContext ctx);
	/**
	 * Enter a parse tree produced by the {@code elementStruktury}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void enterElementStruktury(SuperlangParser.ElementStrukturyContext ctx);
	/**
	 * Exit a parse tree produced by the {@code elementStruktury}
	 * labeled alternative in {@link SuperlangParser#exprBasic}.
	 * @param ctx the parse tree
	 */
	void exitElementStruktury(SuperlangParser.ElementStrukturyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterEqualCond(SuperlangParser.EqualCondContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitEqualCond(SuperlangParser.EqualCondContext ctx);
	/**
	 * Enter a parse tree produced by the {@code notEqualCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterNotEqualCond(SuperlangParser.NotEqualCondContext ctx);
	/**
	 * Exit a parse tree produced by the {@code notEqualCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitNotEqualCond(SuperlangParser.NotEqualCondContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lessThanCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterLessThanCond(SuperlangParser.LessThanCondContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lessThanCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitLessThanCond(SuperlangParser.LessThanCondContext ctx);
	/**
	 * Enter a parse tree produced by the {@code lessOrEqualCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterLessOrEqualCond(SuperlangParser.LessOrEqualCondContext ctx);
	/**
	 * Exit a parse tree produced by the {@code lessOrEqualCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitLessOrEqualCond(SuperlangParser.LessOrEqualCondContext ctx);
	/**
	 * Enter a parse tree produced by the {@code greaterThanCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterGreaterThanCond(SuperlangParser.GreaterThanCondContext ctx);
	/**
	 * Exit a parse tree produced by the {@code greaterThanCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitGreaterThanCond(SuperlangParser.GreaterThanCondContext ctx);
	/**
	 * Enter a parse tree produced by the {@code greaterOrEqualCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void enterGreaterOrEqualCond(SuperlangParser.GreaterOrEqualCondContext ctx);
	/**
	 * Exit a parse tree produced by the {@code greaterOrEqualCond}
	 * labeled alternative in {@link SuperlangParser#condition}.
	 * @param ctx the parse tree
	 */
	void exitGreaterOrEqualCond(SuperlangParser.GreaterOrEqualCondContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void enterIfBlock(SuperlangParser.IfBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void exitIfBlock(SuperlangParser.IfBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#loopBlock}.
	 * @param ctx the parse tree
	 */
	void enterLoopBlock(SuperlangParser.LoopBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#loopBlock}.
	 * @param ctx the parse tree
	 */
	void exitLoopBlock(SuperlangParser.LoopBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#functionBlock}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBlock(SuperlangParser.FunctionBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#functionBlock}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBlock(SuperlangParser.FunctionBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link SuperlangParser#structFields}.
	 * @param ctx the parse tree
	 */
	void enterStructFields(SuperlangParser.StructFieldsContext ctx);
	/**
	 * Exit a parse tree produced by {@link SuperlangParser#structFields}.
	 * @param ctx the parse tree
	 */
	void exitStructFields(SuperlangParser.StructFieldsContext ctx);
}